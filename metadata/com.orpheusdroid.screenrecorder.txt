Categories:System
License:AGPLv3
Web Site:
Source Code:https://github.com/vijai1996/screenrecorder
Issue Tracker:https://github.com/vijai1996/screenrecorder/issues

Summary:lightweight and functional screen recorder
Description:
ScreenCam doesn't need any root access to record your screen and works on all
phones with Android Lollipop 5.0 and above. You can also record audio along with
the screen recording and get it beautifully combined with the recorded video.

Choose from different resolutions, frames per second and bitrate for the best
choice of quality and size of the video or make use of the app shortcut in
android 7.1 nougat or in any custom launcher supporting app shortcuts.
.

Repo Type:git
Repo:https://github.com/vijai1996/screenrecorder

Build:1.1,4
    commit=a078649ef3c3a6b53d16b6987a70d99818094a4a
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
